#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>

// rm -rf seq.out; gcc sequential_computation.c -o seq.out; ./seq.out 

int is_prime(uint32_t num) {
  int ans = 1;
  if (num < 2) {
    ans = 0;
  } else {
    for (int i = 2; i <= sqrt(num) && ans; i++) {
      if(num % i == 0) ans = 0;
    }
  }
  usleep(100);
  return ans;
}

uint32_t *array_prime_calloc(uint32_t limit, uint32_t *count_res) {
  *count_res = 0;
  uint32_t *arr = (uint32_t *)calloc(limit, sizeof(uint32_t));
  for (int i = 0; i < limit; i++) {
    if (is_prime(i)) {
      arr[*count_res] = i;
      (*count_res)++;
    }
  }
  return arr;
}

void print_array(uint32_t arr[], uint32_t n) {
  for (int i = 0; i < n; i++) {
    printf("number %d: %d\n", i, arr[i]);
  }
}

int print_prime_num(uint32_t limit) {
  uint32_t count_prime_num = 0;
  uint32_t *arr = array_prime_calloc(limit, &count_prime_num);
  if (arr == NULL) {
    fprintf(stderr, "err in calloc");
  } else {
    // print_array(arr, count_prime_num);
    free(arr);
  }
  return count_prime_num;
}

int main(int argc, char *argv[]) {
  struct timeval start_time, end_time;
  // Записываем начальное время
  gettimeofday(&start_time, NULL);

  uint32_t n = 100;
  if (argc == 2)
    n = (uint32_t)atoi(argv[1]);
  int count = print_prime_num(n);

  gettimeofday(&end_time, NULL);
  double time_spend = (double)(end_time.tv_sec - start_time.tv_sec) + (double)(end_time.tv_usec - start_time.tv_usec) / 1e6;

  printf("\ncount of prime number up to numbers %d is %d\n", n, count);
  printf("time spend: %lf\n", time_spend);
  return 0;
}