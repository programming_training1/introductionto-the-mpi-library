#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <mpi.h>

int is_prime(int num) {
  int ans = 1;
  if (num < 2) {
    ans = 0;
  } else {
    for (int i = 2; i <= sqrt(num) && ans; i++) {
      if(num % i == 0) ans = 0;
    }
  }
  usleep(100);
  return ans;
}

void print_arr(int arr[], int count) {
  for (int i = 0; i < count; i++) {
    printf("number %d: %d\n", i, arr[i]);
  }
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    
    double start_time, end_time;
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0) {
        start_time = MPI_Wtime(); // Засекаем начальное время на процессе с рангом 0
    }

    int n = 100;  // Диапазон чисел для проверки
    if (argc == 2)
      n = atoi(argv[1]);
    int chunk_size = n / size; // Дипазон чисел для каждого процесса
    int start = rank * chunk_size + 2; //  Число с которого начинаем проверку
    int end = (rank == size - 1) ? n : start + chunk_size - 1; // Число которым заканчиваем проверку

    // Вычисление простых чисел в локальном диапазоне
    int local_primes[chunk_size];
    int local_count = 0;

    for (int i = start; i <= end; i++) {
      if (is_prime(i)) {
          local_primes[local_count] = i;
          local_count++;
      }
    }

    // Собираем результаты на ранге 0
    int count = 0;
    int primes[n];
    if (rank == 0) {

      for(int i = 0; i < local_count; i++) {
        primes[i] = local_primes[i];
        
      }

      count += local_count;
      int local_count_temp;
      for (int proc = 1; proc < size; proc++) {
        MPI_Recv(&local_count_temp, 1, MPI_INT, proc, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(local_primes, local_count_temp, MPI_INT, proc, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for(int i = 0; i < local_count_temp; i++) {
          primes[count + i] = local_primes[i];
        }
        count += local_count_temp;
      }

      // print_arr(primes, count);

    } else {
      MPI_Send(&local_count, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
      MPI_Send(local_primes, local_count, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }

    if (rank == 0) {
      end_time = MPI_Wtime(); // Засекаем конечное время на процессе с рангом 0
      printf("\ncount of prime number up to numbers %d is %d\n", n, count);
      printf("time spend: %lf\n", end_time - start_time);
      
    }

    MPI_Finalize();
    return 0;
}

