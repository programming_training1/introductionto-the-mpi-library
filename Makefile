OBJ_OUTPUT_DIR = obj_result
ARG = 100


par:
	@rm -rf $(OBJ_OUTPUT_DIR)/par.out 
	@mpicc parallel_computing.c -o $(OBJ_OUTPUT_DIR)/par.out
	@mpirun $(OBJ_OUTPUT_DIR)/par.out $(arg)

seq:
	@rm -rf $(OBJ_OUTPUT_DIR)/seq.out
	@gcc sequential_computation.c -o $(OBJ_OUTPUT_DIR)/seq.out
	@$(OBJ_OUTPUT_DIR)/seq.out $(arg)

test:  
	@rm -rf $(OBJ_OUTPUT_DIR)/test.out
	@mpicc test.c -o $(OBJ_OUTPUT_DIR)/test.out
	@mpirun $(OBJ_OUTPUT_DIR)/test.out

clean: 
	@rm -rf $(OBJ_OUTPUT_DIR)/*.out
	@echo "All clean\n"